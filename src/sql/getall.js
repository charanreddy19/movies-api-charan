const { fstat } = require('fs')
const path=require('path')
const fs=require('fs')
const sql=require('mysql')
require('dotenv').config()


const connection=sql.createPool({
    host:process.env.host,
    database:process.env.database,
    user:process.env.user,
    password:process.env.password
})

function queries(query){
    return new Promise((resolve,reject)=>{
        connection.query(query,(err,results,fields)=>{
            if(err){
                reject(err)
            }
            else{
                resolve(results)
            }
        })
    })
}

module.exports.queries=queries