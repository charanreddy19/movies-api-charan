const { fstat } = require('fs')
const path=require('path')
const fs=require('fs')
const sql=require('mysql')
const { CONNREFUSED } = require('dns')
require('dotenv').config()

const connection=sql.createPool({
    host:process.env.host,
    database:process.env.database,
    user:process.env.user,
    password:process.env.password
})

function read(filepath){
    return new Promise((resolve,reject)=>{
        fs.readFile(filepath,'utf8',(err,data)=>{
            if(err){
                reject(err)
            }
            else{
                resolve(JSON.parse(data))
            }
        })
    })
}

function queries(query){
    return new Promise((resolve,reject)=>{
        connection.query(query,(err,results,fields)=>{
            if(err){
                reject(err)
            }
            else{
                resolve(results) 
            }
        })
    })
}


async function insert(table,data,query){
    try{
        let isEmpty=await queries(`select count(*) as count from ${table}`)
        if(isEmpty[0]['count']===0){
            connection.query(query,[data],(err,response)=>{
                if(err){
                    console.error(err)
                }
            })
        }
    }
    catch(err){
        console.error(err)
    }
}




async function push(filepath,table1,table2){
    try{
        let data=await read(filepath)
        let movie=[]
        let directors=[]
        let id
        data.map((movie)=>{
            if(!(directors.includes(movie.Director))){
                directors.push(movie.Director)
            }
            else{

            }
        })
        directors=directors.map((director,index)=>{
            return [index+1,director]
        })
        data.map((movies,index) => {
            directors.forEach((director) => {
                if (director[1] === movies.Director) {
                    id = director[0]
                }
            })
            movie.push([index+1,movies.Title, movies.Description , movies.Runtime , movies.Genre , movies.Rating , movies.Metascore , movies.Votes , movies.Gross_Earning_in_Mil , id , movies.Actor , movies.Year])
        })
        await queries(`CREATE SCHEMA IF NOT EXISTS ${process.env.database}`)
        await queries(`CREATE TABLE IF NOT EXISTS ${table1}(
            id INT PRIMARY KEY AUTO_INCREMENT,
            title Text NOT NULL,
            description_of_movie TEXT NOT NULL,
            runtime INT NOT NULL,
            genre TEXT NOT NULL,
            rating FLOAT NOT NULL,
            metascore TEXT NOT NULL,
            votes BIGINT NOT NULL,
            gross_earning_in_mil TEXT NOT NULL,
            director INT REFERENCES directors(id),
            actor TEXT NOT NULL,
            release_year INT NOT NULL
        )`)
        await insert(table1,movie,`INSERT INTO ${table1} VALUES ?`)
        await queries(`CREATE TABLE IF NOT EXISTS ${table2}(
            id INT PRIMARY KEY AUTO_INCREMENT,
            director_name TEXT NOT NULL
        )`)
        await insert(table2,directors,`INSERT INTO ${table2} VALUES ?`)
    }
    catch(err){
        console.log(err)
    }    
}

module.exports.push=push
