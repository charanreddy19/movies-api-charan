const express=require('express')
const app=express()
const path=require('path')
require('dotenv').config()
const inserts=require(path.join(__dirname,'../sql/exports.js'))
const queries=require(path.join(__dirname,'../sql/getall.js'))


inserts.push(path.join(__dirname,'../data/movies.json'),'movies','directors')


app.use(express.json())
app.use('/',require(path.join(__dirname,'movies.js')))
app.use('/',require(path.join(__dirname,'director.js')))

app.get('/',(req,res)=>{
    res.json({"api/movies":"To get all the movies","api/directors":"To get all the directors","api/movies/id":"For operation on movies table","api/directors/id":"For operations on directors table"})
})

app.listen(process.env.PORT || 8002,()=>{
    console.log('...listening')
})