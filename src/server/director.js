const express=require('express')
const router=express.Router()
const path=require('path')
const inserts=require(path.join(__dirname,'../sql/exports.js'))
const queries=require(path.join(__dirname,'../sql/getall.js'))


router.get('/api/directors',(req,res)=>{
    queries.queries(`select * from directors`)
    .then((results)=>{
        res.json(results)
    })
    .catch((error)=>{
        console.log(error)
        res.status(500).json({msg:'internal server error'})
    })
})

router.get('/api/directors/:id',(req,res)=>{
    let id=req.params.id
    queries.queries(`select * from directors where id=${id}`)
    .then((results)=>{
        if(results.length>0){
            res.json(results)
        }
        else{
            res.status(500).json({msg:`There is no director with the given id ${id}`})
        }
    })
    .catch((error)=>{
        console.log(error)
        res.status(500).json({msg:'internal server error'})
    })
})

router.post('/api/directors',(req,res)=>{
    let body=req.body
    let check=0
    if(!body.director_name && typeof(body.director_name!=='string')){
        check+=1
    }
    if(check===0){
        queries.queries(`select * from directors where director_name="${body.director_name}"`)
        .then((results)=>{
            if(results.length>0){
                res.status(500).json({msg:`director_name with ${body.director_name}  already exists`})
            }
            else{
                queries.queries(`insert into directors(director_name) values("${body.director_name}")`)
                .then(()=>{
                    res.json({msg:"director inserted success fully"})
                })
                .catch((error)=>{
                    console.log(error)
                    res.status(500).json({msg:'internal server error'})
                })
            }
        })
        .catch((error)=>{
            console.log(error)
            res.status(500).json({msg:'internal server error'})
        })
    }
    else{
        res.status(500).json({msg:"check data type and null values before inserting"})
    }
})

router.put('/api/directors/:id',(req,res)=>{
    let id=req.params.id
    queries.queries(`select * from directors where id=${id}`)
    .then((results)=>{
        if(results.length>0){
            let body=req.body
            let check=0
            if(!body.director_name && typeof(body.director_name!=='string')){
                check+=1
            }
            if(check===0){
                queries.queries(`select * from directors where director_name="${body.director_name}"`)
                .then((results)=>{
                    if(results.length>0){
                        res.status(500).json({msg:`director_name with ${body.director_name}  already exists`})
                    }
                    else{
                        queries.queries(`update directors set director_name="${body.director_name}" where id=${id}`)
                        .then(()=>{
                            res.json({msg:"director updated successfully"})
                        })
                        .catch((error)=>{
                            console.log(error)
                            res.status(500).json({msg:'internal server error'})
                        })
                    }
                })
                .catch((error)=>{
                    console.log(error)
                    res.status(500).json({msg:'internal server error'})
                })
            }
            else{
                res.status(500).json({msg:"check data type and null values before inserting"})
            }
        }
        else{
            res.status(500).json({msg:`No director with the id ${id}`})
        }
    })
})

router.delete('/api/directors/:id',(req,res)=>{
    let id=req.params.id
    let check=0
    if(!id && typeof(Number(id))!=="number"){
        check+=1
    }
    if(check===0){
        queries.queries(`select * from directors where id="${id}"`)
        .then((results)=>{
            if(results.length>0){
                queries.queries(`delete from directors where id="${id}"`)
                .then(()=>{
                    res.json({msg:"Director deleted successfully"})
                })
                .catch((error)=>{
                    console.log(error)
                    res.status(500).json({msg:`internal server error`}) 
                })
            }
            else{
                res.status(500).json({msg:`No movies with id ${id}`})
            }
        })
        .catch((error)=>{
            console.log(error)
            res.status(500).json({msg:`internal server error`})
        })
    }
    else{
        res.status(500).json({msg:"check null values and data types"})
    }
})

module.exports=router