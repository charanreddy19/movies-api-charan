const express=require('express')
const router=express.Router()
const path=require('path')
const inserts=require(path.join(__dirname,'../sql/exports.js'))
const queries=require(path.join(__dirname,'../sql/getall.js'))

router.get('/api/movies',(req,res)=>{
    queries.queries('select * from movies')
    .then((data)=>{
        res.json(data)
    })
    .catch((err)=>{
        console.log(err)
    })
})

router.post('/api/movies',(req,res)=>{
    let body=Object.entries(req.body)
    let keys=''
    let keysArr=[]
    let values=''
    let valuesArr=[]
    body.map((element,index)=>{
        if(index===0){
            keys+=element[0]
            values+=element[1]
            keysArr.push(element[0])
            valuesArr.push(element[1])
        }
        else{
            keys+=','+element[0]
            values+=','+element[1]
            keysArr.push(element[0])
            valuesArr.push(element[1])
        }
    })
    console.log(typeof(valuesArr[2]))
    if(keysArr.length===11){
        if(keysArr[0] == 'title' && keysArr[1] == 'description_of_movie' && keysArr[2] == 'runtime' && keysArr[3] == 'genre' && keysArr[4] == 'rating' && keysArr[5] == 'metascore' && keysArr[6] == 'votes' && keysArr[7] == 'gross_earning_in_mil' && keysArr[8] == 'director' && keysArr[9] == 'actor' && keysArr[10]==='release_year'){
            let checkEmpty=0
            valuesArr.filter((data)=>{
                if(!data){
                    checkEmpty+=1
                }
            })
            if(checkEmpty===0){
                if(typeof(valuesArr[2])==="number" && typeof(valuesArr[4])==="number" && typeof(valuesArr[6])==="number" && typeof(valuesArr[8])==="number" && typeof(valuesArr[10])==="number"){
                    queries.queries(`select * from movies where title="${valuesArr[0]}"`)
                    .then((results)=>{
                        if(results.length===0){
                            queries.queries(`insert into movies(${keys}) values("${valuesArr[0]}","${valuesArr[1]}","${valuesArr[2]}","${valuesArr[3]}","${valuesArr[4]}","${valuesArr[5]}","${valuesArr[6]}","${valuesArr[7]}","${valuesArr[8]}","${valuesArr[9]}","${valuesArr[10]}")`)
                            .then(()=>{
                                res.json({msg:'Movie inserted successfully'})
                            })
                            .catch((error)=>{
                                console.log(error)
                                res.json({msg:'Internal server error'})
                            })
                        }
                        else{
                            res.status(500).json({msg:`movie name with ${valuesArr[0]} already exists`})
                        }
                    })
                    .catch((err)=>{
                        console.log(err)
                        res.status(500).json({msg:"Internal server error"})
                    })
                
                }
                else{
                    res.status(500).json({msg:"Entered the correct sequence.But value data types are incorrect. Data types for runtime,rating,votes,director,year are integers"})
                }
            }
            else{
                res.status(500).json({msg:"empty values not allowed"})
            }
        }
        else{
            res.status(500).json({msg:`Enter all values in correct order like title,description_of_movie,runtime,genre,rating,metascore,votes,gross_earning_in_mil,director,actor,release_year`})
        }
    }
    else
    {
        res.status(500).json({msg:`Enter all the required details in order as title,description_of_movie,runtime,genre,rating,metascore,votes,gross_earning_in_mil,director,actor,release_year`})
    }
})

router.get('/api/movies/:id',(req,res)=>{
    queries.queries(`select * from movies where id=${req.params.id}`)
    .then((results)=>{
        if(results.length<1){
            res.status(500).json({msg:`movie with id ${req.params.id} not exists`})
        }
        else{
            res.json(results)
        }
    })
    .catch((error)=>{
        console.log(error)
        res.json({msg:'Internal server error'})
    })
})

router.put('/api/movies/:id',(req,res)=>{
    id=req.params.id
    console.log(id)
    queries.queries(`select * from movies where id=${id}`)
    .then((results)=>{
        if(results.length===0){
            res.status(500).json({msg:`Movie with id ${id} not exists`})
        }
        else{
            let body=Object.entries(req.body)
            let keys=''
            let keysArr=[]
            let values=''
            let valuesArr=[]
            body.map((element,index)=>{
                if(index===0){
                    keys+=element[0]
                    values+=element[1]
                    keysArr.push(element[0])
                    valuesArr.push(element[1])
                }
                else{
                    keys+=','+element[0]
                    values+=','+element[1]
                    keysArr.push(element[0])
                    valuesArr.push(element[1])
                }
            })
            if(keysArr.length===11){
                if(keysArr[0] == 'title' && keysArr[1] == 'description_of_movie' && keysArr[2] == 'runtime' && keysArr[3] == 'genre' && keysArr[4] == 'rating' && keysArr[5] == 'metascore' && keysArr[6] == 'votes' && keysArr[7] == 'gross_earning_in_mil' && keysArr[8] == 'director' && keysArr[9] == 'actor' && keysArr[10]==='release_year'){
                    let checkEmpty=0
                    valuesArr.filter((data)=>{
                        if(!data){
                            checkEmpty+=1
                        }
                    })
                    if(checkEmpty===0){
                        if(typeof(valuesArr[2])==="number" && typeof(valuesArr[4])==="number" && typeof(valuesArr[6])==="number" && typeof(valuesArr[8])==="number" && typeof(valuesArr[10])==="number"){
                            queries.queries(`select * from movies where id="${id}"`)
                            .then((results)=>{
                                if(results.length>0){
                                    console.log(id)
                                    queries.queries(`update movies set ${keysArr[0]}="${valuesArr[0]}",${keysArr[1]}="${valuesArr[1]}",${keysArr[2]}="${valuesArr[2]}",${keysArr[3]}="${valuesArr[3]}",${keysArr[4]}="${valuesArr[4]}",${keysArr[5]}="${valuesArr[5]}",${keysArr[6]}="${valuesArr[6]}",${keysArr[7]}="${valuesArr[7]}",${keysArr[8]}="${valuesArr[8]}",${keysArr[9]}="${valuesArr[9]}",${keysArr[10]}="${valuesArr[10]}" where id=${id}`)
                                    .then(()=>{
                                        res.json({msg:'Movie updated successfully'})
                                    })
                                    .catch((error)=>{
                                        console.log(error)
                                        res.json({msg:'Internal server error'})
                                    })
                                }
                                else{
                                    res.status(500).json({msg:`movie name with ${valuesArr[0]} not exists`})
                                }
                            })
                            .catch((err)=>{
                                console.log(err)
                                res.status(500).json({msg:"Internal server error"})
                            })
                        }
                        else{
                            res.status(500).json({msg:"Entered the correct sequence.But value data types are incorrect. Data types for runtime,rating,votes,director,year are integers"})
                        }
                    }
                    else{
                        res.status(500).json({msg:"empty values are not allowed"})
                    }
                }
                else{
                    res.status(500).json({msg:`Enter all values in correct order like title,description_of_movie,runtime,genre,rating,metascore,votes,gross_earning_in_mil,director,actor,release_year`})
                }
            }
            else
            {
                res.status(500).json({msg:`Enter all the required details in order as title,description_of_movie,runtime,genre,rating,metascore,votes,gross_earning_in_mil,director,actor,release_year`})
            }
        }
    })
    .catch((error)=>{
        console.log(error)
        res.status(500).json({msg:"internal server error"})
    })
})


router.delete('/api/movies/:id',(req,res)=>{
    let id=req.params.id
    let check=0
    if(!id && typeof(Number(id))!=="number"){
        check+=1
    }
    if(check===0){
        queries.queries(`select * from movies where id=${id}`)
        .then((results)=>{
            if(results.length===0){
                res.status(500).json({msg:`movie with id ${id} not exists`})
            }
            else{
                queries.queries(`delete from movies where id=${id}`)
                .then(()=>{
                    res.json({msg:"Movie deleted successfully"})
                })
                .catch(()=>{
                    res.status(500).json({msg:"internal server error"})
                })
            }
        })
        .catch((error)=>{
            console.log(error)
            res.status(500).json({msg:"internal server error"})
        })
    }
    else{
        res.status(500).json({msg:"check null values and data types of id"})
    }
})

module.exports=router